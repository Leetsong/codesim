package hash

import (
	"fmt"
)

type Rolling struct {
	prime  int
	k      int

	// intermediate results
	primeSKM1 int64  // prime^(k-1)
	lastHash  int64  // last saved hash
	lastBytes []byte // last accepted bytes
}

func New(prime, k int) *Rolling {
	return &Rolling{
		prime, k,
		int64(0),
		int64(0),
		nil,
	}
}

func (r *Rolling) Reset() {
	r.lastBytes = nil
}

func (r *Rolling) Next(b []byte) int64 {
	hash := int64(0)

	if r.lastBytes == nil {
		tmpHash, primeSKM1 := r.renew(b)
		hash = tmpHash
		r.primeSKM1 = primeSKM1
	} else {
		hash = r.next(b)
	}

	r.lastBytes = b
	r.lastHash = hash

	return r.lastHash
}

func (r Rolling) renew(b []byte) (hash, primeSKM1 int64) {
	if len(b) != r.k {
		panic(fmt.Sprintf("Only accept bytes with length %d", r.k))
	}
	hash, primeSKM1 = int64(0), 1
	for i := r.k - 1; i >= 0; i -- {
		hash += int64(b[i]) * primeSKM1
		primeSKM1 *= int64(r.prime)
	}
	return hash, primeSKM1 / int64(r.prime)
}

func (r Rolling) next(b []byte) int64 {
	if len(b) != r.k {
		panic(fmt.Sprintf("Only accept bytes with length %d", r.k))
	}
	return (r.lastHash - int64(r.lastBytes[0]) * r.primeSKM1) * int64(r.prime) + int64(b[r.k - 1])
}
