package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io.github.leetsong/codesim/hash"
	"os"
	"os/exec"
	"path"
	"regexp"
	"strings"
)

const (
	debugVerbose = false
	debugCompile = true
)

var (
	patternSections     *regexp.Regexp
	patternComments     *regexp.Regexp
	patternSpaces       *regexp.Regexp
	patternPunctuations *regexp.Regexp
	patternLabels       *regexp.Regexp
)

func init() {
	sections, err := regexp.Compile(`\.(?:(?:section)|(build_version)|(globl))\s+.*`)
	if err != nil {
		panic(err)
	}
	patternSections = sections

	comments, err := regexp.Compile(`##.*`)
	if err != nil {
		panic(err)
	}
	patternComments = comments

	spaces, err := regexp.Compile(`\s+`)
	if err != nil {
		panic(err)
	}
	patternSpaces = spaces

	punctuations, err := regexp.Compile(`[~!@#$%^&*()\-+_=\[\]{}\\|;:'",.?<>/]+`)
	if err != nil {
		panic(err)
	}
	patternPunctuations = punctuations

	labels, err := regexp.Compile(`.*?:`)
	if err != nil {
		panic(err)
	}
	patternLabels = labels
}

type fingerprintItem struct {
	Hash  int64
}

type Fingerprint []fingerprintItem

type WinnowModel struct {
	K     int // gram size
	T     int // threshold size
	B     int // hash prime number
	hash  *hash.Rolling

	Paths []string

	bitCodePaths []string  // bitcode files
	contents     [][]byte  // file index -> []byte (each is a byte)
	kGramHashes  [][]int64 // file index -> []string (each is a hash value of a kGram)

	fingerprintIndices [][]int    // file index -> []int (each is a index of its kGramHashes)
}

func New(k, t, b int, files []string) *WinnowModel {
	wm := &WinnowModel{
		k, t, b,
		hash.New(b, k),
		files,
		make([]string, len(files)),
		make([][]byte, len(files)),
		make([][]int64, len(files)),
		make([][]int, len(files)),
	}
	wm.init()
	return wm
}

func (wm *WinnowModel) init() {
	stopWords := []*regexp.Regexp{
		patternSections,
		patternComments,
		patternLabels,
		patternSpaces,
		patternPunctuations,
	}

	for which := range wm.Paths {
		// compile to llvm bitcode with text format
		wm.compileToBitcode(which)
		// a first pass over the file transforms it to
		// eliminate undesirable differences between documents
		wm.readAndSanitize(which, stopWords)
		// a second pass over the data splits it to
		// k-grams, and computes their hashes
		wm.computeHashes(which)
		// a third pass over the data splits the kgrams
		// into windows and select the hashes
		wm.computeFingerprints(which)
	}
}

func (wm *WinnowModel) compileToBitcode(which int) {
	//noinspection GoBoolExpressions
	if !debugCompile {
		wm.bitCodePaths[which] = wm.Paths[which]
		return
	}
	fPath := wm.Paths[which]
	fName := path.Base(fPath)
	bitcodePath := "/tmp/" + fName + ".bc"
	cmd := exec.Command("clang", "-o", bitcodePath, "-S", "-O3", "-std=c++11", fPath)
	err := cmd.Run()
	if err != nil {
		panic(err)
	}
	wm.bitCodePaths[which] = bitcodePath
}

func (wm *WinnowModel) readAndSanitize(which int, stopWords []*regexp.Regexp) {
	var path = wm.bitCodePaths[which]
	var buffer bytes.Buffer

	file, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer func() {
		if err := file.Close(); err != nil {
			panic(err)
		}
	}()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		line = strings.TrimSpace(line)
		for _, s := range stopWords {
			line = s.ReplaceAllString(line, "")
		}
		buffer.WriteString(line)
	}

	wm.contents[which] = buffer.Bytes()
}

func (wm *WinnowModel) computeHashes(which int) {
	var (
		kGramNum    = wm.GetKGramNum(which)
		kGramHashes = make([]int64, kGramNum)
	)

	wm.hash.Reset()
	for i := 0; i < kGramNum; i ++ {
		kGramHashes[i] = wm.hash.Next(wm.GetKGram(which, i))
	}

	wm.kGramHashes[which] = kGramHashes

	//noinspection GoBoolExpressions
	if debugVerbose {
		fmt.Printf("=> Hashes for %s\n", wm.Paths[which])
		for i := 0; i < kGramNum; i ++ {
			fmt.Printf("[%d] \"%s\" %d\n", i, wm.GetKGram(which, i), kGramHashes[i])
		}
	}
}

func (wm *WinnowModel) computeFingerprints(which int) {
	var (
		kGramNum     = wm.GetKGramNum(which)
		windowSize   = wm.GetWindowSize()
		kGramHashes  = wm.kGramHashes[which]
		fingerprint  = make([]int, 0)
		prevMinIndex = -1
	)

	for i := 0; i < kGramNum - windowSize; i += windowSize {
		localMinIndex := i
		for j := i + 1; j < i + windowSize; j ++ {
			currentHash := kGramHashes[j]
			if currentHash <= kGramHashes[localMinIndex] {
				localMinIndex = j
			}
		}
		if prevMinIndex == -1 {
			prevMinIndex = localMinIndex
			fingerprint = append(fingerprint, prevMinIndex)
		} else if kGramHashes[prevMinIndex] != kGramHashes[localMinIndex] {
			prevMinIndex = localMinIndex
			fingerprint = append(fingerprint, prevMinIndex)
		}
	}

	wm.fingerprintIndices[which] = fingerprint

	//noinspection GoBoolExpressions
	if debugVerbose {
		fmt.Printf("=> Fingerprints (%d) for: %s\n", len(fingerprint), wm.Paths[which])
		for i := 0; i < len(fingerprint); i ++ {
			fmt.Printf("[%d] \"%s\" %d\n",
				fingerprint[i],
				wm.GetKGram(which, fingerprint[i]),
				kGramHashes[fingerprint[i]])
		}
	}
}

func (wm *WinnowModel) GetWindowSize() int {
	return wm.T - wm.K + 1
}

func (wm *WinnowModel) GetKGramNum(which int) int {
	return len(wm.contents[which]) - wm.K + 1
}

func (wm *WinnowModel) GetKGram(which, index int) []byte {
	length := len(wm.contents[which])
	start := index
	end := start + wm.K
	if end >= length {
		end = length
	}
	return wm.contents[which][start:end]
}

func (wm *WinnowModel) GetFingerprint(which int) Fingerprint {
	var (
		fingerprintIndices = wm.fingerprintIndices[which]
		kGramHashes        = wm.kGramHashes[which]
		fingerprint        = make(Fingerprint, 0, len(fingerprintIndices))
	)

	for _, index := range fingerprintIndices {
		fingerprint = append(fingerprint, fingerprintItem{
			kGramHashes[index],
		})
	}

	return fingerprint
}

func (wm *WinnowModel) GetSim(a, b int) float64 {
	// use a Jaccard Similarity variant
	var (
		fpOfA    = wm.GetFingerprint(a)
		fpOfB    = wm.GetFingerprint(b)
		fpSetOfA = make(map[fingerprintItem]bool)
		fpSetOfB = make(map[fingerprintItem]bool)
		jointNum = 0
		minNum   = 0
	)

	for _, fpi := range fpOfA {
		fpSetOfA[fpi] = true
	}

	for _, fpi := range fpOfB {
		if _, ok := fpSetOfA[fpi]; ok {
			if _, ok := fpSetOfB[fpi]; !ok {
				jointNum += 1
			}
		}
		fpSetOfB[fpi] = true
	}

	minNum = len(fpSetOfA)
	if len(fpSetOfB) < minNum {
		minNum = len(fpSetOfB)
	}

	//noinspection GoBoolExpressions
	if debugVerbose {
		// print jointNum, minNum
		fmt.Printf("=> jointNum: %d, minNum: %d\n", jointNum, minNum)
	}

	return float64(jointNum) / float64(minNum) * 100
}

func main() {
	if len(os.Args) < 3 {
		panic("Insufficient arguments, at least 2")
	}
	fmt.Printf("%.02f\n",
		New(5, 8, 5, []string{os.Args[1], os.Args[2]}).GetSim(0,1))
}
