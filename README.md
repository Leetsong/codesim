## CodeSim

### 1 实验介绍

本实验是《软件工程研究导引》的第一个实验，旨在利用程序分析的手段对克隆代码进行检测。

### 2 代码克隆介绍

研究人员认为常见的代码克隆类型包括四类 [1]：

##### 2.1 Type-1 

除了空白符、代码排版和注释外，语法等价的代码片段。

##### 2.2 Type-2

除了变量名、字面量的值、空白符、代码排版和注释外，语法等价的代码片段。

##### 2.3 Type-3

语句层面语法相似的代码片段。代码片段间存在语句的增加、修改和删除。

##### 2.4 Type-4

语法不相似但实现了相同功能的代码片段。

### 3 算法实现

##### 3.1 代码指纹 - wonnow

本文利用 Schleimer 提出的 winnow 算法 [2] 对代码克隆进行检测。Winnow 算法是基于文本指纹的克隆检测，通过对文本进行指纹抽取和比较实现检测。由于 winnow 算法及其相关算法对文本进行 N-Gram 划分，因此能够很好地解决 Type-3 类型的克隆检测。

然而，对代码视为文本并直接采用 winnow 算法进行检测，将无法消除变量名、代码、注释的影响，同时也难于处理采用了宏/函数抽取的克隆代码片段。

为解决上述问题，我们，

1. 首先利用 Clang 对 Cxx 代码进行编译（-S）和优化（-O3）得到其对应的汇编代码
2. 对汇编代码进行过滤，从而消除空白符、标签、注释以及 section 等元信息
3. 将处理后的代码作为文本输入 winnow 算法从而得到代码的指纹

##### 3.2 指纹相似度 - Jaccard Variant

对于得到 winnow 指纹的两份代码，由于 winnow 指纹以集合的形式存在，因此常用的集合间距离 "Jaccard Distance" 可以被用来计算集合间的距离，进而得到相似度:
$$
similarity(a, b) = \mathcal{J}(winnow(a), winnow(b)) = \frac{|winnow(a) \cap winnow(b)|}{|winnow(a) \cup winnow(b)|} * 100
$$
然而，当代码片段 a 为代码片段 b 中很小的一部分时，上述距离将产生很小的相似度，因此，我们采用如下距离来得到相似度：
$$
similarity(a, b) = \frac{|winnow(a) \cap winnow(b)|}{\min{(winnow(a), winnow(b))}} * 100
$$


### 4 代码实现

> 要求：
>
> 1. Go: 版本 >= 1.11.5
> 2. Clang (只需要二进制文件)

#### 1 安装

利用一下命令将codesim目录放到 `$GOPATH` 下：

```bash
# 如果不想“污染”自己的 Go 环境，在任意目录下执行此命令
$ export GOPATH=`pwd`
$ mkdir -p $GOPATH/src/io.github.leetsong
$ mv codesim $GOPATH/src/io.github.leetsong/
```

#### 2 运行

利用以下命令运行：

``` bash
$ cd $GOPATH/src/io.github.leetsong/codesim
$ go run main.go <first_cc> <second_cc>
```

### 5 参考文献

[1] Jeffrey Svajlenko, Judith F. Islam, Iman Keivanloo, Chanchal K. Roy and Mohammad Mamun Mia, "Towards a Big Data Curated Benchmark of Inter-Project Code Clones", In *Proceedings of the Early Research Achievements track of the 30th International Conference on Software Maintenance and Evolution* (ICSME 2014), 5 pp., Victoria, Canada, September 2014.

[2] Saul Schleimer, Daniel S. Wilkerson, and Alex Aiken. 2003. Winnowing: local algorithms for document fingerprinting. In *Proceedings of the 2003 ACM SIGMOD international conference on Management of data* (SIGMOD '03). ACM, New York, NY, USA, 76-85. DOI: https://doi.org/10.1145/872757.872770